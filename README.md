# meta-fridge-obj-detect-demo

This is the OpenEmbedded/Yocto layer for an object detection application. The
layer is based on the [meta-renesas-ai layer](https://github.com/renesas-rz/meta-renesas-ai).

## BSP Dependencies

This layer is built upon the following packages:
- [Renesas VLP v2.1.3](https://mp.renesas.com/en-us/rzg/marketplace/linux_package/rzg-vlp-eva.html)
- meta-renesas-ai v3.2.0

This project comes with template files to make it easier for the user to quickly
setup the build environment. Only specific platforms are supported, therefore
template files are machine specific and can be found under:
**templates/${MACHINE}**

Copying local.conf and bblayers.conf from the templates directory to your
build/conf directory is usually the first thing the user wants to do, but the
configuration must be inspected and further customized according to the project
requirements.

## Build Instructions

Please follow the build instructions in the Renesas RZ/G VLP Release Notes until
the build step (NOTE: the Release Notes can be found [here](https://mp.renesas.com/en-us/rzg/marketplace/linux_package/rzg-vlp-eva.html)).

Before running `bitbake core-image-weston`, carry out the following steps
(making sure you have set the $WORK environment variable to the directory with
the VLP meta-layers):

1) Clone the meta-renesas-ai layer
```
cd $WORK
git clone -b v3.2.0 https://github.com/renesas-rz/meta-renesas-ai.git
```

2) Clone the fridge-app repository
```
cd $WORK
git clone -b v1.0 https://gitlab.com/fridge-demo/fridge-app.git
```

3) Set up the build environment (if you haven't already)
```
cd $WORK
source poky/oe-build-env
```

4) Copy the local.conf and bblayers.conf templates into your conf directory
```
cp ${WORK}/meta-fridge-obj-detect-demo/templates/<machine>/* ${WORK}/build/conf/
```
Note: in the case of the RZ/G1H board, machine = iwg21m

5) Start the build
```
cd ${WORK}/build
bitbake core-image-weston
```

The build should produce the following files needed to flash onto the board:
- uImage-iwg21m.bin (kernel)
- r8a7742-iwg21m.dtb (device tree)
- core-image-weston-iwg21m.tar.gz (RFS)

--------------------------------------------------------------------------------

This project is licensed under the terms of the MIT license (please see file
COPYING.MIT in this directory for further details).
