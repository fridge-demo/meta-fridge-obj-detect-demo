#!/bin/sh

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib:/usr/local/lib/:/usr/lib/gstreamer-1.0/
export PYTHONPATH=$PYTHONPATH:/usr:/home/root/fridge-app/labelmap
export GST_REGISTRY=/root/.gstreamer-1.0/
export XDG_RUNTIME_DIR=/run/user/0
export LD_PRELOAD=/usr/lib/libEGL.so

echo "Starting Fridge demo..."
/home/root/fridge-app/object_detect.py &
