SUMMARY = "Fridge Object Detection App Demo Recipe"
DESCRIPTION = "The fridge object detection app showcases a real world use case \
of e-AI. The app is designed such that it takes a picture with a USB camera \
and processes it, displaying the image with bounding boxes, identities and \
confidence scores around the items present in the image."
LICENSE = "MIT"

LIC_FILES_CHKSUM = "file://COPYING.MIT;md5=5eba1999d32d7ff55b42a17e78136bdf"

PV="1.1"

SRC_URI = " \
	git://gitlab.com/fridge-demo/fridge-app.git;protocol=https; \
	file://icons \
	file://launch-demo.sh \
	file://kill-demo.sh \
	file://models \
"
SRCREV = "7173fda88edbcd9e8bb42782ec4fb30038d5bbec"

S = "${WORKDIR}/git"

DEMO_INSTALL_DIR = "/home/root"

FILES_${PN} += "${DEMO_INSTALL_DIR}"

DEPENDS += " \
	python-numpy \
	python-numpy-native \
	opencv \
"


RDEPENDS_${PN} = " \
	python \
	python-pip \
	python-setuptools \
	python-opencv \
	python-numpy \
"

do_install () {
    install -d ${D}/${DEMO_INSTALL_DIR}/fridge-app
    cp -r ${S}/* ${D}/${DEMO_INSTALL_DIR}/fridge-app/

    install -d ${D}/${DEMO_INSTALL_DIR}/icons
    install -m 0644 ${S}/../icons/* ${D}/${DEMO_INSTALL_DIR}/icons

    install -m 0755 ${S}/../*.sh ${D}/${DEMO_INSTALL_DIR}

    install -d ${D}/${DEMO_INSTALL_DIR}/models/fridge-demo/
    cp -r ${S}/../models/* ${D}/${DEMO_INSTALL_DIR}/models/fridge-demo/
}
